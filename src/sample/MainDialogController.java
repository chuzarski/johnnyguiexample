package sample;

import javafx.beans.value.ChangeListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;

import javax.xml.soap.Text;
import java.awt.event.MouseEvent;

/**
 * @author Cody Huzarski <chuzarski@gmail.com> (Tassit)
 */
public class MainDialogController {

    @FXML private ToggleGroup searchByToggle;
    @FXML private TextField lastNameField;
    @FXML private TextField usernameField;
    @FXML private TextField employeeIdField;
    @FXML private RadioButton lastNameRadio;
    @FXML private RadioButton usernameRadio;
    @FXML private RadioButton employeeRadio;

    public MainDialogController() {
    }

    @FXML
    protected void initialize() {
        initToggleListener();
        initFieldEvents();
    }

    private void initFieldEvents() {

        // when focused
        lastNameField.focusedProperty().addListener((value, hasOld, isNew) -> {
            if(isNew)
                lastNameRadio.setSelected(true);
        });
        usernameField.focusedProperty().addListener((value, hasOld, isNew) -> {
            if(isNew)
                usernameRadio.setSelected(true);
        });
        employeeIdField.focusedProperty().addListener((value, hasOld, isNew) -> {
            if (isNew)
                employeeRadio.setSelected(true);
        });

    }

    private void startListingWindow(String searchTerm) {

    }

    private void initToggleListener() {
        searchByToggle.selectedToggleProperty().addListener((observableValue, oldToggle, newToggle) -> {
            RadioButton newRadio = (RadioButton) newToggle;
            RadioButton oldRadio = (RadioButton) oldToggle;

            // set field state
            switch (newRadio.getId()) {
                case "lastNameRadio":
                    lastNameField.requestFocus();
                    break;
                case "usernameRadio":
                    usernameField.requestFocus();
                    break;
                case "employeeRadio":
                    employeeIdField.requestFocus();
                    break;
            }
        });
    }
}
