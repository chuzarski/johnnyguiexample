package sample;

import javafx.fxml.FXML;
import javafx.scene.control.ListView;

/**
 * @author Cody Huzarski <chuzarski@gmail.com> (Tassit)
 */
public class EmployeeListingController {

    @FXML ListView<EmployeeRecord> employeeList;

    @FXML
    protected void initialize() {

    }
}
